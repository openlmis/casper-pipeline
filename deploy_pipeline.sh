#!/usr/bin/env bash
set -e

export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://casper-elmis.a.openlmis.org:2376"
export DOCKER_CERT_PATH="${PWD}/credentials"

cp -r $credentials ./credentials

/usr/local/bin/docker-compose down -v
if [ "$RESET_CACHE" == "reset" ]; then
  echo "Resetting cache, remove volume"
  docker volume inspect mapping-volume &>/dev/null && docker volume rm mapping-volume
else
  echo "Persisting cache, keep volume"
fi
docker network inspect casper_elmis_pipeline_bridge &>/dev/null || docker network create --driver bridge casper_elmis_pipeline_bridge
docker volume create --name=mapping-volume #ignored if it already exists
/usr/local/bin/docker-compose up \
    --build \
    --force-recreate \
    -d