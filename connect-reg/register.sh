#!/bin/sh

set -e

until curl -f "http://connect:8083/"; do
  >&2 echo -e "\nDebezium Connect is unavailable - sleeping"
  sleep 10
done

>&2 echo -e "\nDebezium Connect is up - registering connectors"
cd /connectors

tmp=$(mktemp)

echo "Replacing host, port, username, password, database of register-v2-source.json file"
jq --arg h2 "${V2_PG_HOST}" '.config."database.hostname" = $h2' register-v2-source.json > "$tmp" && mv "$tmp" register-v2-source.json
jq --arg p2 "${V2_PG_PORT}" '.config."database.port" = $p2' register-v2-source.json > "$tmp" && mv "$tmp" register-v2-source.json
jq --arg u2 "${V2_PG_USERNAME}" '.config."database.user" = $u2' register-v2-source.json > "$tmp" && mv "$tmp" register-v2-source.json
jq --arg pw2 "${V2_PG_PASSWORD}" '.config."database.password" = $pw2' register-v2-source.json > "$tmp" && mv "$tmp" register-v2-source.json
jq --arg d2 "${V2_PG_DB}" '.config."database.dbname" = $d2' register-v2-source.json > "$tmp" && mv "$tmp" register-v2-source.json

echo "Replacing host, port, username, password, database of register-v3-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-sink.json > "$tmp" && mv "$tmp" register-v3-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-sink.json > "$tmp" && mv "$tmp" register-v3-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-sink.json > "$tmp" && mv "$tmp" register-v3-sink.json

echo "Replacing host, port, username, password, database of register-v3-requisition-group-members-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-requisition-group-members-sink.json > "$tmp" && mv "$tmp" register-v3-requisition-group-members-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-requisition-group-members-sink.json > "$tmp" && mv "$tmp" register-v3-requisition-group-members-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-requisition-group-members-sink.json > "$tmp" && mv "$tmp" register-v3-requisition-group-members-sink.json

echo "Replacing host, port, username, password, database of register-v3-supported-programs-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-supported-programs-sink.json > "$tmp" && mv "$tmp" register-v3-supported-programs-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-supported-programs-sink.json > "$tmp" && mv "$tmp" register-v3-supported-programs-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-supported-programs-sink.json > "$tmp" && mv "$tmp" register-v3-supported-programs-sink.json

echo "Replacing host, port, username, password, database of register-v3-role-rights-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-role-rights-sink.json > "$tmp" && mv "$tmp" register-v3-role-rights-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-role-rights-sink.json > "$tmp" && mv "$tmp" register-v3-role-rights-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-role-rights-sink.json > "$tmp" && mv "$tmp" register-v3-role-rights-sink.json

echo "Replacing host, port, username, password, database of register-v3-dispensable-attributes-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-dispensable-attributes-sink.json > "$tmp" && mv "$tmp" register-v3-dispensable-attributes-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-dispensable-attributes-sink.json > "$tmp" && mv "$tmp" register-v3-dispensable-attributes-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-dispensable-attributes-sink.json > "$tmp" && mv "$tmp" register-v3-dispensable-attributes-sink.json

echo "Replacing host, port, username, password, database of register-v3-orderables-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-orderables-sink.json > "$tmp" && mv "$tmp" register-v3-orderables-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-orderables-sink.json > "$tmp" && mv "$tmp" register-v3-orderables-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-orderables-sink.json > "$tmp" && mv "$tmp" register-v3-orderables-sink.json

echo "Replacing host, port, username, password, database of register-v3-facility-type-approved-products-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-facility-type-approved-products-sink.json > "$tmp" && mv "$tmp" register-v3-facility-type-approved-products-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-facility-type-approved-products-sink.json > "$tmp" && mv "$tmp" register-v3-facility-type-approved-products-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-facility-type-approved-products-sink.json > "$tmp" && mv "$tmp" register-v3-facility-type-approved-products-sink.json

echo "Replacing host, port, username, password, database of register-v3-available-requisition-column-sources-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-available-requisition-column-sources-sink.json > "$tmp" && mv "$tmp" register-v3-available-requisition-column-sources-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-available-requisition-column-sources-sink.json > "$tmp" && mv "$tmp" register-v3-available-requisition-column-sources-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-available-requisition-column-sources-sink.json > "$tmp" && mv "$tmp" register-v3-available-requisition-column-sources-sink.json

echo "Replacing host, port, username, password, database of register-v3-column-maps-sink.json file"
jq --arg h3 "${V3_PG_HOST}" --arg p3 "${V3_PG_PORT}" --arg d3 "${V3_PG_DB}" '.config."connection.url" = "jdbc:postgresql://"+$h3+":"+$p3+"/"+$d3+"?stringtype=unspecified"' register-v3-column-maps-sink.json > "$tmp" && mv "$tmp" register-v3-column-maps-sink.json
jq --arg u3 "${V3_PG_USERNAME}" '.config."connection.user" = $u3' register-v3-column-maps-sink.json > "$tmp" && mv "$tmp" register-v3-column-maps-sink.json
jq --arg pw3 "${V3_PG_PASSWORD}" '.config."connection.password" = $pw3' register-v3-column-maps-sink.json > "$tmp" && mv "$tmp" register-v3-column-maps-sink.json


./register-v3-sink.sh
./register-v2-source.sh
./register-v3-requisition-group-members-sink.sh
./register-v3-supported-programs-sink.sh
./register-v3-role-rights-sink.sh
./register-v3-dispensable-attributes-sink.sh
./register-v3-orderables-sink.sh
./register-v3-facility-type-approved-products-sink.sh

./register-v3-available-requisition-column-sources-sink.sh
./register-v3-column-maps-sink.sh
