
## v2 db connections

- there are 2 processor groups that have v2 db connections, whose credentials aren't
    set:  `Transform POD line items v2 to v3` and 
    `Transform Facility Type Approved Products v2 to v3`.
- go to each one, find the processor with the sql connection hit configure,
    find the database connection pooling service under `properties`, then hit
    arrow button, and disable that controller server.  Configure and update the
    connection string and the password.  Then re-enable the service.


## ids and upserts

There are some flows, e.g. the Geographic Levels flow, that can make it from
source to sink, and still fail to update the database even though the sink is
an "upsert" operation.  This is because the flow doesn't take into account that
the data in the sink might already be there (e.g. the same level), but with a
different id from a different run (e.g. Redis was restarted and lost the data).

- this could be helped by keeping Redis cache more (volume permanance), but this
    is already gennerally done, and doesn't address the pipeline being run against
    a sink that has permanent data.
- a real fix would need to change the flow, to be more aware of the sink state.
    This is expensive as the flows would need to understand what else in the data
    is unique aside from the ID.

## connect sink failure

- There have been instances where the sink fails, and yet the items are in
    the kafka sink topic.  This might be because connect batches writes, and if
    one fails, it doesn't attempt the others in the batch, and it doesn't
    appear there's an easy way to have the others retry.

## resetting a consumer

It's sometimes useful to reset the offset that a consumer as at, e.g. to retry a
a nifi flow.

To access from inside the kafka container:

```
docker exec -it <kafka-container> bash
```

To list consumer groups:
```
kafka-consumer-groups --bootstrap-server localhost:29092 --list
```

Example out:
```
connect-v3-role-rights-sink
connect-v3-supported-programs-sink
nifi-gzone-consumer-group
connect-v3-orderables-sink
connect-v3-dispensable-attributes-sink
nifi-glvl-consumer-group
connect-v3-column_maps-sink
connect-v3-available-requisition-column-sources-sink
connect-v3-facility-type-approved-products-sink
connect-v3-requisition-group-members-sink
```

To see the offsets and active connections of a group:

```
kafka-consumer-groups --bootstrap-server localhost:29092 --group <consumer_group> --describe
```

To reset a nifi consumer:

1. First stop the process group in Nifi
1. Run e.g. `kafka-consumer-groups --bootstrap-server localhost:29092 --group <consumer_group> --all-topics --reset-offsets --to-earliest --execute`
    substritute the right consumer group, e.g. `nifi-glvl-consumer-group`

## to list and get redis keys

Note all nifi process groups use the `0` database, the default.

```
docker exec -it <redis-container> redis-cli -h localhost
keys *
get <key-name>
```

example out:
```
1) "adjustmentreasonSTOLEN"
 2) "geographiczone437"
 3) "adjustmentreasonPASSED_OPEN_VIAL_TIME_LIMIT"
 4) "rightVIEW_REPORTING_RATE_REPORT"
 5) "rightVIEW_ORDER_REPORT"
 6) "rightFACILITY_FILL_SHIPMENT"
 7) "rightVIEW_STOCK_ON_HAND"
 8) "rightMANAGE_SCHEDULE"
 9) "rightCREATE_ORDER_REQUISITION"
10) "adjustmentreasonLOST"
11) "adjustmentreasonEXPIRED"
12) "geographiclevel1"
13) "adjustmentreasonTRANSFER_OUT"
14) "rightVIEW_ORDER_REQUISITION"
15) "adjustmentreasonCOLD_CHAIN_FAILURE"
16) "rightVIEW_REQUISITION"
17) "rightVIEW_ORDER"
18) "rightMANAGE_PROGRAM_PRODUCT"
19) "rightDELETE_REQUISITION"
20) "rightDELETE_EQUIPMENT_INVENTORY"
21) "rightMANAGE_USER"
22) "rightCOMPLETE_POD"
23) "rightCONVERT_TO_ORDER"
24) "geographiclevel3"
25) "rightMANAGE_SETTING"
26) "rightMANAGE_EQUIPMENT_INVENTORY"
27) "rightMANAGE_FACILITY_APPROVED_PRODUCT"
28) "rightMANAGE_SUPERVISED_EQUIPMENTS"
29) "rightCREATE_REQUISITION"
30) "rightVIEW_AVERAGE_CONSUMPTION_REPORT"
31) "adjustmentreasonCLINIC_RETURN"
32) "rightCONFIGURE_RNR"
33) "rightMANAGE_STOCK"
34) "rightVIEW_ADJUSTMENT_SUMMARY_REPORT"
35) "rightSYSTEM_SETTINGS"
36) "rightMANAGE_PRODUCT"
37) "geographiclevel4"
38) "rightMANAGE_GEOGRAPHIC_ZONE"
39) "adjustmentreasonDAMAGED"
40) "rightMANAGE_EQUIPMENT_SETTINGS"
41) "rightMANAGE_REQUISITION_GROUP"
42) "rightINITIALIZE_STOCK"
43) "rightMANAGE_SUPERVISORY_NODE"
44) "rightMANAGE_SUPPLY_LINE"
45) "rightMANAGE_CUSTOM_REPORTS"
46) "rightAPPROVE_REQUISITION"
47) "rightMANAGE_ROLE"
48) "rightVIEW_CONSUMPTION_REPORT"
49) "adjustmentreasonTRANSFER_IN"
50) "rightMANAGE_SUPPLYLINE"
51) "geographiclevel2"
52) "rightVIEW_TIMELINESS_REPORT"
53) "rightMANAGE_REPORT"
54) "rightMANAGE_FACILITY"
55) "rightAUTHORIZE_REQUISITION"
```

## redis remove keys by pattern

```
docker exec -it <redis-container> redis-cli -h localhost
```

## process group start order

1. geo levels
1. geo zones


## troubleshooting

- Issue: There have been instances where the entities are in the sink topic, 
    and yet the items are not in the v3 database.
    - Solution: This might be because connect batches writes in SQL, and if
    one fails, it doesn't attempt the others in the batch.  To look for this
    run `docker logs <connect-container>` and look for insert failures.  If the
    failure is due to a key conflict, the conflict could be deleted manually via
    SQL, and then the connect container can be restarted.  If this still doesn't
    work, it's possible to reset the sink offset.
- 
