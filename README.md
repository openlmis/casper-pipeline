# Project Casper (WIP)

Project Casper is the ["friendly" ghost migration](https://en.wikipedia.org/wiki/Casper_the_Friendly_Ghost).
It demonstrates the possibility of streaming changes from an OpenLMIS v2 system
to v3 automatically, in near real-time. It does this using Kafka,
Kafka Connect, Debezium, Nifi and Redis.

This is still a work-in-progress and very much a proof of concept.

## Getting Started

Steps to get it working:

1. Get a working OpenLMIS v2 system running, with Postgres at `elmis:5432`, username `postgres` and password `p@ssw0rd`.
2. Ensure your v2 Postgres is configured for Debezium, [instructions](https://debezium.io/docs/connectors/postgresql/#setting-up-PostgreSQL).
3. Start the OpenLMIS v3 Ref Distro.
4. In a new terminal, run `./start-local.sh` to start Kafka, Kafka Connect, Nifi and Redis. This will also register the connectors.
5. Go to Nifi at `localhost:8080/nifi` and ensure all of the processors in each group are enabled and running.
6. Now, any changes you make to requisitions in the v2 system should automatically register in v3.

To see what is stored in Kafka, you can go to Kafka Topics UI at `localhost:8000` to see the topics. Data for v2 starts with `original.requisition.` and for v3 `requisition.`.

To see the connectors, go to Kafka Connect at `localhost:8083`. You can check the status of the v2 connector at `localhost:8083/connectors/v2-source/status` and the v3 connector at `localhost:8083/connectors/v3-sink/status`.

For monitoring, access the Grafana dashboard at `localhost:3000`. The default 
username and password are `admin`/`admin`. Grafana by default has dashboards for
monitoring both Kafka and NiFi

The Nifi template/process group is responsible for the transforms. Some v2-v3
mapping files, which are used for this proof of concept, can be found in the
`config/services/nifi/casper` folder. These mappings could be potentially be stored in Redis.

## Project Structure

The pipeline uses Docker to start up all of the components. There are two Docker Compose files, one for local development, and the other for automated deployment. They have corresponding shell scripts `start-local.sh` and `deploy_pipeline.sh` to start up Docker in their respective environments.

Each Docker Compose file describes the same pipeline components:

* `config-container` - this holds all of the configuration files under the `config` folder and stores in in the volume `config-volume`, which is then used by other components.
* `zookeeper` and `kafka` - Zookeeper is the backend for Kafka. Kafka provides streaming for the pipeline through topics.
* `kafka-rest` and `kafka-topics-ui` - these provide a nice Web UI interface for Kafka topics.
* `connect` - Kafka Connect, which works on top of Kafka to provide a platform for connectors between Kafka and other data sources. In this case, we use it for a "source" connector, streaming data from eLMIS, and a "sink" connector, streaming data to OpenLMIS v3.
* `connect-reg` - this component is what automatically registers the source and sink connectors.
* `nifi` - the transform part of the ETL pipeline. It takes the topics from eLMIS that are in eLMIS schema and transforms it into v3 schema that can be upserted into the v3 system. This is done through a series of Nifi processors, which are called processor groups. Each eLMIS table (requisitions, requisition_line_items, etc.) has its own processor group in Nifi.
* `redis` - a semi-persistent datastore used by Nifi to store some key-value pairs. It is semi-persistent through the docker volume `mapping-volume`.
* `grafana` - the webapp used for displaying monitoring dashboards
* `influxdb` - Used to store monitoring information for NiFi. It is semi-persistent through the docker volume `influxdb-volume`
* `prometheus` - Used to store monitoring information for Kafka
* `log` and `scalyr` - for logging.

### Subfolders

* `config/services/nifi` - configuration for Nifi. The `casper` subfolder holds all of the basic mapping (from eLMIS to v3) files. The `scripts` subfolder hold the scripts and definition files that automatically load and start the controller services and process groups on Nifi startup.
* `connect-reg` - contains the definition for the two Kafka Connect connectors. This is where they would be modified if necessary.
* `config/services/nifi/scripts/preload/templates` - contains the XML templates for the Nifi process groups. The ones in the Nifi Registry (http://nifi-registry.openlmis.org:18080) the actual ones that are loaded into Nifi on startup, but these are backups stored in source control. 
