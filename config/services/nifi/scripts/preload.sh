#!/bin/bash

export NIFI_BASE_URL="http://nifi:8081"
export WORKING_DIR="/config/nifi/scripts"
export TEMPLATES_DIR="$WORKING_DIR/preload/templates"
export NIFI_UP_RETRY_COUNT=240

main() {
  if waitNifiAvailable ${NIFI_UP_RETRY_COUNT}; then
    local subCommand=$1

    if [ "$subCommand" == "init" ]; then
      initialize "${@:2}"
    else
      return 1
    fi
  else
    return 1
  fi

  return $?
}

waitNifiAvailable() {
  echo "PRELOAD Waiting for NiFi to be available"
  local maxTries=$1
  local retryCount=1

  while ! curl -s -f "$NIFI_BASE_URL/nifi"; do
    sleep 10
    retryCount=$((retryCount + 1))
    if [[ "$retryCount" -gt "$maxTries" ]]; then
      echo "PRELOAD ERROR, too many retries waiting for NiFi to be available"
      return 1
    fi
  done

  return 0
}

initialize() {
  uploadAndImportTemplates "$@"
  restartFlows "$@"
}

uploadAndImportTemplates() {
  echo "PRELOAD Uploading and importing templates"
  local returnCode=0
  local templateCount=0

  for templateFile in ${TEMPLATES_DIR}/*.xml; do
    local filename=$(basename ${templateFile})
    if [ -e "${templateFile}" ]; then
      echo "PRELOAD Uploading template defined in ${templateFile}"
      templateId=$(curl -s $NIFI_BASE_URL/nifi-api/process-groups/root/templates/upload -F template=@${templateFile} | xmlstarlet sel -t -v '/templateEntity/template/id')
      #echo "PRELOAD templateId = ${templateId}"

      if [ ! "$templateId" == "" ]; then
        echo "PRELOAD Creating process group by importing template defined in ${templateFile}"
        local offsetCount=$((templateCount + 4))
        local processGroupOffsetX=$((offsetCount / 5 * 400))
        local processGroupOffsetY=$((offsetCount % 5 * 200))
        curl -s -X POST -H 'Content-Type: application/json' $NIFI_BASE_URL/nifi-api/process-groups/root/template-instance -d '{"templateId":"'"${templateId}"'","originX":"'"${processGroupOffsetX}"'","originY":"'"${processGroupOffsetY}"'"}' > /dev/null
        templateCount=$((templateCount + 1))
      else
        echo "PRELOAD ERROR Uploading template was not successful"
        returnCode=2
      fi
    fi
  done

  return $returnCode
}

restartFlows() {
  echo "PRELOAD Configuring process groups and starting Flows"
  
  # Enter sensitive values
  echo "PRELOAD Set password for v3 db for root process group"
  curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq '.controllerServices|keys[]' | while read key ;
  do
    controllerServiceId=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq -r ".controllerServices[$key].component.id")
    controllerServiceName=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq -r ".controllerServices[$key].component.name")

    if [ "$controllerServiceName" == "DBCPConnectionPool" ] || [ "$controllerServiceName" == "OLMISDBCPConnectionPool" ];
    then
      curl -s -X PUT -H 'Content-Type: application/json' -d '{"revision":{"clientId":"random", "version":"0"},"component":{"id":"'"${controllerServiceId}"'","properties":{"Database Connection URL":"'"jdbc:postgresql://$2:$3/open_lmis?stringtype=unspecified"'","Database User":"'"$4"'","Password":"'"$5"'"}}}' $NIFI_BASE_URL/nifi-api/controller-services/${controllerServiceId} > /dev/null
    else
      continue
    fi
  done

  # Enable controller services
  echo "PRELOAD Enable controller services for root process group"
  curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq '.controllerServices|keys[]' | while read key ;
  do
    controllerServiceId=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq -r ".controllerServices[$key].component.id")

    versionNumber=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/controller-services/${controllerServiceId} | jq -r ".revision.version")
    curl -s -X PUT -H 'Content-Type: application/json' -d '{"revision":{"clientId":"random", "version":"'"${versionNumber}"'"},"component":{"id":"'"${controllerServiceId}"'","state":"ENABLED"}}' $NIFI_BASE_URL/nifi-api/controller-services/${controllerServiceId} > /dev/null
  done

  # Check all controller services are enabled before continuing
  echo "PRELOAD Check controller services are enabled for root process group"
  curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq '.controllerServices|keys[]' | while read key ;
  do
    controllerServiceId=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq -r ".controllerServices[$key].component.id")
    controllerServiceName=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/flow/process-groups/root/controller-services | jq -r ".controllerServices[$key].component.name")

    runStatus=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/controller-services/${controllerServiceId} | jq -r ".status.runStatus")
    while [ ! "$runStatus" == "ENABLED" ]; do
      echo "PRELOAD Controller service ${controllerServiceName} is not enabled, waiting"
      sleep 5
      runStatus=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/controller-services/${controllerServiceId} | jq -r ".status.runStatus")
    done

    echo "PRELOAD Controller service ${controllerServiceName} is enabled"
  done

  # Start all process groups
  echo "PRELOAD Start all process groups"
  curl -s -X GET $NIFI_BASE_URL/nifi-api/process-groups/root/process-groups | jq '.[]|keys[]' | while read key ; 
  do
    processorGroupId=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/process-groups/root/process-groups | jq -r ".[][$key].component.id")
    processorGroupName=$(curl -s -X GET $NIFI_BASE_URL/nifi-api/process-groups/root/process-groups | jq -r ".[][$key].component.name")

    # Start process group
    # echo "PRELOAD Start process group ${processorGroupName}"
    # curl -s -X PUT -H 'Content-Type: application/json' -d '{"id":"'"${processorGroupId}"'","state":"RUNNING"}' $NIFI_BASE_URL/nifi-api/flow/process-groups/${processorGroupId} > /dev/null
  done
}

main "$@" &
exit $?
