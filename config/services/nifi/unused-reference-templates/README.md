# Reference Templates

The NiFi templates in this directory are "unused", in the sense that they are
not automatically loaded into NiFi anywhere. However, these templates are used
as components of the templates that are automatically loaded into NiFi
(located under `config/services/nifi/scripts/preload/templates/`).

## LogLineageToInfluxDB.xml

This NiFi process group records to InfluxDB metadata about the things routed to
it.

For more background on NiFi flowfiles, see https://nifi.apache.org/docs/nifi-docs/html/nifi-in-depth.html#provenance-repository

Configuration:
 - The process group defines one custom variable, `measurementName`, which you
   should manually set to be unique for each instance of this process group that
   points to the same InfluxDB database. You can edit this variable by right
   clicking on the process group and then clicking "Variables".
 - Inside this process group, the "PutInfluxDB" process group has some options
   for connecting to InfluxDB, but the settings there currently should work
   out of the box with this Casper pipeline. "InfluxDB connection URL" should
   point to your running InfluxDB instance, and "Database Name" should refer
   to an existing database in InfluxDB. Multiple instances of this process group
   can share the same database.
 - To get more granular data about how long small pieces of the flow take, you
   can route data into a single process group at multiple points along the flow.
   There is currently no way to distinguish these however, besides sorting them
   by time.

Input: Arbitrary NiFi flowfiles can be routed to this process group. This
process group will use the `filename` and `lineageStartDate` attributes of
flowfiles routed to it---both of these should be present by default on all
flowfiles.

Output: Measurements recorded to InfluxDB have three tags.
 - duration: the time in milliseconds since the oldest parent of the input
   flowfile was created
 - fileID: (weakly tested) If you have instrumented multiple points along the
   same flow, this identifier can let you associate all the measurements that
   correspond to the same piece of data going through the flow. Caution: if you
   mess around with changing the filename attribute in part of your flow, you
   might end up with the wrong number of measurements for a single fileID,
   either too many or too few.
 - lineageStartDate: (unused) the time the oldest parent of the input flowfile
   was created
