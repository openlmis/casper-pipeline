#!/bin/bash

docker-compose -f docker-compose.local.yml \
    exec kafka /usr/bin/kafka-topics \
    --zookeeper zookeeper:32181 \
    --delete \
    --topic $1
